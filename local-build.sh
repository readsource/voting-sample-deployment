#!/bin/bash
docker build -t readsourceacr.azurecr.io/voting-system-sample/worker:0.0.0-local ../worker-app/dotnet
docker build -t readsourceacr.azurecr.io/voting-system-sample/results:0.0.0-local ../results-app/dotnet
docker build -t readsourceacr.azurecr.io/voting-system-sample/vote:0.0.0-local ../voting-app/dotnet
